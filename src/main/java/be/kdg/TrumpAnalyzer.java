package be.kdg;

import org.apache.spark.SparkConf;
import org.apache.spark.streaming.Duration;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaReceiverInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.twitter.*;
import scala.Tuple5;
import twitter4j.Status;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class TrumpAnalyzer {
    SparkConf conf;
    JavaStreamingContext ssc;

    public TrumpAnalyzer(boolean forceLocal, int batchDuration) {
        conf = new SparkConf().setAppName("TrumpAnalyzer");
        if (forceLocal) {
            conf.setMaster("local[3]");
        }

        ssc = new JavaStreamingContext(conf, new Duration(batchDuration));
    }

    public void readTwitterStream(String[] filters, String output) {

        JavaReceiverInputDStream<Status> stream = TwitterUtils.createStream(ssc, filters);

        JavaDStream<Tuple5<String, String, Timestamp, Long, Boolean>> words = stream.flatMap(s -> {
            List<Tuple5<String, String, Timestamp, Long, Boolean>> list = new ArrayList<>();
            for (String w : s.getText().split(" ")) {
                list.add(new Tuple5<>(w.toLowerCase()
                        .replaceAll("\\p{Punct}", "")
                        .replaceAll("\\d", "")
                        .trim(), s.getUser().getName(), new Timestamp(s.getCreatedAt().getTime()), s.getId(), w.startsWith("#")));
            }
            return list.iterator();
        });

        // List<String> stopWords = StopWords.getWords();    //Does not work on AWS
        List<String> stopWords = Arrays.asList("i", "me", "my", "myself", "we", "our", "ours", "ourselves", "you", "your", "yours", "yourself", "yourselves", "he", "him", "his", "himself", "she", "her", "hers", "herself", "it", "its", "itself", "they", "them", "their", "theirs", "themselves", "what", "which", "who", "whom", "this", "that", "these", "those", "am", "is", "are", "was", "were", "be", "been", "being", "have", "has", "had", "having", "do", "does", "did", "doing", "a", "an", "the", "and", "but", "if", "or", "because", "as", "until", "while", "of", "at", "by", "for", "with", "about", "against", "between", "into", "through", "during", "before", "after", "above", "below", "to", "from", "up", "down", "in", "out", "on", "off", "over", "under", "again", "further", "then", "once", "here", "there", "when", "where", "why", "how", "all", "any", "both", "each", "few", "more", "most", "other", "some", "such", "no", "nor", "not", "only", "own", "same", "so", "than", "too", "very", "s", "t", "can", "will", "just", "don", "should", "now");


        JavaDStream<String> record = words
                .filter(w -> !stopWords.contains(w._1()))
                .filter(w -> w._1().length() > 2)
                .filter(w -> !w._1().startsWith("http"))
                .map(w -> new PorterStemmer().stemWord(w._1() + ";" + w._2() + ";" + w._3() + ";" + w._4() + ";" + w._5()));

        record.dstream().saveAsTextFiles(output, "");

        ssc.start();
        try {
            ssc.awaitTermination();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
