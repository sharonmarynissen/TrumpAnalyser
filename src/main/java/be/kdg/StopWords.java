package be.kdg;

import java.io.*;
import java.util.List;
import java.util.ArrayList;

public class StopWords implements Serializable {
    public static final long serialVersionUID = 42L;
    private List<String> stopWords;
    private static StopWords _singleton;

    private StopWords() {
        this.stopWords = new ArrayList<>();
        BufferedReader rd = null;
        try {
            rd = new BufferedReader(
                    new InputStreamReader(
                            new FileInputStream(
                                    new File("src/resources/stopwords.txt"))));
            String line = null;
            while ((line = rd.readLine()) != null)
                this.stopWords.add(line);
        } catch (IOException ex) {
        } finally {
            try {
                if (rd != null) rd.close();
            } catch (IOException ex) {
            }
        }
    }

    private static StopWords get() {
        if (_singleton == null)
            _singleton = new StopWords();
        return _singleton;
    }

    public static List<String> getWords() {
        return get().stopWords;
    }
}
